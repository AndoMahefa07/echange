create database takalo;
use takalo;

create table user(
    id int primary key auto_increment,
    nom varchar(50),
    email varchar(50),
    password varchar(50)
);

create table category(
    id int primary key auto_increment,
    nom varchar(50)
);

create table objet(
    id int primary key auto_increment,
    nom varchar(50),
    description text,
    prix double precision,
    idcategory int,
    iduser int
);

create table photo(
    id int primary key auto_increment,
    nom varchar(50),
    idobjet int
);

create table proposition(
    id int primary key auto_increment,
    idobjet1 int,
    idobjet2 int
);

create table actionproposition(
    idproposition int,
    etat varchar(50)
);

alter table objet add foreign key (idcategory) references category(id);
alter table objet add foreign key (idcategory) references category(id);
alter table objet add foreign key (idcategory) references category(id);
alter table actionproposition add foreign key (idproposition) references proposition(id);
insert into user values
(0,'root','root@gmail.com','root')
;
insert into category values
(null,'Vêtement')
;
insert into category values
(null,'DVD')
;
insert into category values
(null,'Livre')
;
insert into category values
(null,'Bijoux')
;
update user set id='0' where nom='root';

create or replace view recherche as 
select o.*, c.nom as nomCate, c.id as idCate 
    from objet as o
    join category as c
    on c.id = o.idcategory;
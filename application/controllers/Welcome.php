<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/home');
		$this->load->view('Templates/footer');
	}
	public function home() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/home');
		$this->load->view('Templates/footer');
	}
	public function categories() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$data['id'] = $_GET['idcate'];
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/categories',$data);
		$this->load->view('Templates/footer');
	}		
	public function business() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/business');
		$this->load->view('Templates/footer');
	}
	public function contact() {
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();f
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/contact');
		$this->load->view('Templates/footer');
	}		
}
	